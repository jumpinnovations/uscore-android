package ph.jumpdigital.innovations.uscore.Fragments.Payment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.github.mikephil.charting.charts.LineChart;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.PaymentCenterModel;
import ph.jumpdigital.innovations.uscore.Singleton.UserModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.VolleyRequest;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class BillerCenterMainActivity extends AppCompatActivity {
    JSONObject jsonObject;
    EditText refNumEdt, statementDateEdt, dueDateEdt, dateOfPaymentEdt,
            billerEdt, AmountEdt, paymentCenter;
    Button billerNextBtn;

    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_biller_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Add Billing");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        refNumEdt = (EditText) findViewById(R.id.refNumEdt);
        statementDateEdt = (EditText) findViewById(R.id.statementDateEdt);
        dueDateEdt = (EditText) findViewById(R.id.dueDateEdt);
        dateOfPaymentEdt = (EditText) findViewById(R.id.dateOfPaymentEdt);
        billerEdt = (EditText) findViewById(R.id.billerEdt);
        AmountEdt = (EditText) findViewById(R.id.AmountEdt);
        paymentCenter = (EditText) findViewById(R.id.paymentCenter);

        billerNextBtn = (Button) findViewById(R.id.billerNextBtn);
        billerNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBiller();
            }
        });

    }


    private void addBiller() {
        String url = Constants.apiURL + Constants.payment_bill;
        Constants.volleyTAG = "add_bill";

        HashMap<String, String> params = new HashMap<>();
        params.put("user_id", UserModel.getUserID());
        params.put("reference_number", refNumEdt.getText().toString());
        params.put("statement_date", statementDateEdt.getText().toString());
        params.put("due_date", dueDateEdt.getText().toString());
        params.put("date_of_payment", dateOfPaymentEdt.getText().toString());
        params.put("biller", billerEdt.getText().toString());
        params.put("amount", AmountEdt.getText().toString());
        params.put("payment_center", paymentCenter.getText().toString());


        VolleyRequest request = new VolleyRequest(BillerCenterMainActivity.this,
                Constants.postMethod, url,
                params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                BillerCenterMainActivity.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(BillerCenterMainActivity.this, "Loading", "Please wait...");

    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                Toast.makeText(BillerCenterMainActivity.this, error.getLocalizedMessage() + error.getMessage(), Toast.LENGTH_LONG).show();
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Paymentgateway-Success", response.toString(4));
                    CustomUtils.hideDialog();
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");
                    if (Constants.status.toString().contentEquals("true")) {
                        if (Constants.volleyTAG.toString().contentEquals("add_bill")) {
                            startActivity(new Intent(BillerCenterMainActivity.this, AddReceiptActivity.class));
                            PaymentCenterModel.setPaymentGatewayID(jsonObject.getString("id"));

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
