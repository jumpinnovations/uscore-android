package ph.jumpdigital.innovations.uscore.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.Drawer.MainNavigationDrawer;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.UserModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.SharedPref;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.VolleyRequest;

import static ph.jumpdigital.innovations.uscore.Utilities.Constants.status;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class LoginMainActivity extends AppCompatActivity {
    private Intent i;
    public static JSONObject jsonObject;
    private SharedPref sharedPref;

    EditText usernameLoginEdt, passwordLoginEdt;
    Button signInBtn;
    ImageView unionBankLogo; //for debugging
    LinearLayout createAccnt;

    String username, password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_layout);
        sharedPref = new SharedPref(LoginMainActivity.this);
        usernameLoginEdt = (EditText) findViewById(R.id.usernameLoginEdt);
        passwordLoginEdt = (EditText) findViewById(R.id.passwordLoginEdt);
        passwordLoginEdt.setText("qweasdzxc");
        if (sharedPref.getUserName() != null) {
            usernameLoginEdt.setText(sharedPref.getUserName());
        }

        unionBankLogo = (ImageView) findViewById(R.id.unionBankLogo);
        unionBankLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginMainActivity.this, MainNavigationDrawer.class));
            }
        });

        signInBtn = (Button) findViewById(R.id.signInBtn);
        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = usernameLoginEdt.getText().toString();
                password = passwordLoginEdt.getText().toString();
                signInUser();
            }
        });

        createAccnt = (LinearLayout) findViewById(R.id.createAccnt);
        createAccnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginMainActivity.this, SignUpMainActivity.class));
            }
        });
    }

    private void signInUser() {
        String url = Constants.apiURL + Constants.loginUrl;
        HashMap<String, String> params = new HashMap<>();
        params.put("user_name", username);
        params.put("password", password);
        Constants.volleyTAG = "login";
        VolleyRequest request = new VolleyRequest(LoginMainActivity.this,
                Constants.postMethod, url,
                params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                LoginMainActivity.this, true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(LoginMainActivity.this, "Loading", "Logging in...");
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                Toast.makeText(LoginMainActivity.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(LoginMainActivity.this, "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Login-Success", response.toString(4));
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    status = response.getBoolean("status");
                    UserModel.setUserID(jsonObject.getString("user_id"));
                    sharedPref.setUserName(jsonObject.getString("user_name"));
                    sharedPref.setAUTHToken(jsonObject.getString("auth_token"));

                    if (status.toString().contentEquals("true")) {
                        startActivity(new Intent(LoginMainActivity.this, MainNavigationDrawer.class));
                        LoginMainActivity.this.finish();
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        CustomUtils.hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

}
