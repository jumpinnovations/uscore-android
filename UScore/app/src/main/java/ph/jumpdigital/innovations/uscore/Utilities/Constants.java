package ph.jumpdigital.innovations.uscore.Utilities;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */
/**/
public class Constants {

    public static String volleyTAG = "";
    public static Boolean status = null;
    public static final String PACKAGE_NAME = "ph.jumpdigital.innovations.uscore";
    public static final String baseURL = "http://54.175.132.161:3000/";
    public static final String apiURL = baseURL + "api/v1/";
    public static final String signUp = "users/sign_up";
    public static final String loginUrl = "users/sign_in";
    public static final String payment_bill = "payment_gateways";
    public static final String uploadReceiptImg = "payment_gateways/update_image";
    public static final String add_union_accnt_num = "users/update_unionbank_accnt_num";
    public static final String showUserDetais = "user";


    //VISA
    //API Keys/Shared Secret(X-Pay Token) - VISA
    public static final String VISA_API_KEY = "NUQV4PC92OQNPZ2J0L5S2104llnp29sCOawEZ7dJZO6YQ8aNw";
    public static final String VISA_SHARED_SECRET = "U-tJUXNqmNn25z0Q0Qk3+1{yl5eQm{$Pw-3-4{m#";

    public static final String VISA_USER_ID = "Y40VRE286WC47PWH5SIR211gtgKlY1lqzsnftdCShsZRoyiHU";
    public static final String VISA_PASSWORD = "8Ifu34Q9WPK66d0bls4TSH4sPpX6";

    public static final String VISA_ACCOUNT_NUM = "4957030420210512";
    public static final String VISA_RETRIEVAL_NUM = "330000550000";
    public static final String VISA_SYSTEM_TRACE = "451006";

    public static final String VISA_COUNTRY_CODE_CHAR = "PH";
    public static final String VISA_COUNTRY_CODE_NUM_CURRENCY = "608";
    public static final String VISA_PHONECALLING_CODE = "63";

    // Header
    //  Accept: application/json
    //  Authorization: {base64 encoded userid:password}
    public static final String VISA_GENERAL_INQUIRY_URL = "https://sandbox.api.visa.com/paai/generalattinq/v1/cardattributes/generalinquiry";
    public static String VISA_FUND_TRANSFER_INQUIRY_URL = "https://sandbox.api.visa.com/paai/fundstransferattinq/v1/cardattributes/fundstransferinquiry";

    //UNIONBANK
    public static final String UNION_CLIENT_SECRET_KEY = "pT8hX0aF2qE7gM6mX3kC8cU1vC3uA0iQ5nB4sN0iL1xC8uH8oT";
    public static final String UNION_CLIENT_ID = "5db1fa08-85a8-4cd1-a744-18ad1f01b5e7";

    public static final String UNION_BASEURL_API = "https://api.us.apiconnect.ibmcloud.com/ubpapi-dev/sb/api/";
    public static final String UNION_GET_ACCOUNT_URL = UNION_BASEURL_API + "RESTs/getAccount?account_no=";
    public static final String UNION_GET_BRANCHES_URL = UNION_BASEURL_API + "RESTs/getBranches";
    public static final String UNION_GET_ATMS_URL = UNION_BASEURL_API + "RESTs/getATMs";

    public static final String UNION_CHANNEL_ID = "UHACK_0015";
    public static final String UNION_ACCNT_NUM_1 = "000000014651";
    public static final String UNION_ACCNT_NUM_2 = "000000014652";
    public static final String UNION_ACCNT_NUM_3 = "000000014653";
    public static final String UNION_ACCNT_NUM_4 = "000000014654";
    public static final String UNION_ACCNT_NUM_5 = "000000014655";
    public static final String UNION_ACCNT_NUM_6 = "000000014656";
    public static final String UNION_ACCNT_NUM_7 = "000000014657";
    public static final String UNION_ACCNT_NUM_8 = "000000014658";
    public static final String UNION_ACCNT_NUM_9 = "000000014659";
    public static final String UNION_ACCNT_NUM_10 = "000000014660";


    //GOOGLE
    public static final String GOOGLE_MAP_API_KEY = "AIzaSyB2_7hDtkkwixXQOdiQ5DiiePMekbgIw3c";

    //HTTP METHOD
    public static final int getMethod = com.android.volley.Request.Method.GET;
    public static final int postMethod = com.android.volley.Request.Method.POST;
    public static final int putMethod = com.android.volley.Request.Method.PUT;
    public static final int delMethod = com.android.volley.Request.Method.DELETE;


}
