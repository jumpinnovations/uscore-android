package ph.jumpdigital.innovations.uscore.Singleton;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vidalbenjoe on 28/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class PaymentCenterModel {
    public static String getPaymentGatewayID() {
        return paymentGatewayID;
    }

    public static void setPaymentGatewayID(String paymentGatewayID) {
        PaymentCenterModel.paymentGatewayID = paymentGatewayID;
    }

    public static String paymentGatewayID;

    public static String getCurrency() {
        return currency;
    }

    public static void setCurrency(String currency) {
        PaymentCenterModel.currency = currency;
    }

    public static String getAccount_name() {
        return account_name;
    }

    public static void setAccount_name(String account_name) {
        PaymentCenterModel.account_name = account_name;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        PaymentCenterModel.status = status;
    }

    public static String getAvaiable_balance() {
        return avaiable_balance;
    }

    public static void setAvaiable_balance(String avaiable_balance) {
        PaymentCenterModel.avaiable_balance = avaiable_balance;
    }

    public static String getCurrent_balance() {
        return current_balance;
    }

    public static void setCurrent_balance(String current_balance) {
        PaymentCenterModel.current_balance = current_balance;
    }

    public static String getUaccountNum() {
        return UaccountNum;
    }

    public static void setUaccountNum(String uaccountNum) {
        UaccountNum = uaccountNum;
    }

    public static String UaccountNum;
    public static String currency;
    public static String account_name;
    public static String status;
    public static String avaiable_balance;
    public static String current_balance;

    public static ArrayList<HashMap<String, String>> getArraylist() {
        return arraylist;
    }

    public static void setArraylist(ArrayList<HashMap<String, String>> arraylist) {
        PaymentCenterModel.arraylist = arraylist;
    }

    public static ArrayList<HashMap<String, String>> arraylist;

}
