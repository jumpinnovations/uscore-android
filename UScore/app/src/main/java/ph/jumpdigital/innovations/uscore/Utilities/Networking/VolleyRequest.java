package ph.jumpdigital.innovations.uscore.Utilities.Networking;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ph.jumpdigital.innovations.uscore.Utilities.Constants;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class VolleyRequest {
    Context context;
    public JsonObjectRequest jsonObjectRequest;
    SharedPref sharedPref;
    public static Boolean isUnionBank;

    public VolleyRequest(Context context, int method, String url, HashMap<String, String> params, JSONObject jsonParams, Response.Listener<JSONObject> reponseListener,
                         Response.ErrorListener errorListener, Activity activity, final Boolean requiredToken, final String volleyTAG) {
        this.context = context;
        sharedPref = new SharedPref(context);
        if (params != null) {
            Log.i("VolleyHashMap", params.toString());
        } else {

        }

        if (volleyTAG.equals("fetchuserprofile") || volleyTAG.equals("getuserdata")) {
            //THIS IS FOR METHOD GET
            jsonObjectRequest = new JsonObjectRequest(url, jsonParams, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-type", "application/json");
                    headers.put("Accept", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    } else {

                    }

                    return headers;
                }
            };
        } else if (volleyTAG.equals("")) {

            //THIS IS FOR METHOD PUT AND POST USING JSONOBJECT FORMAT
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, jsonParams, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", "");
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        } else {
            //THIS IS FOR METHOD PUT AND POST USING HASHMAP FORMAT
            jsonObjectRequest = new JsonObjectRequest(method,
                    url, new JSONObject(params), reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Accept", "application/json");
                    if (requiredToken.equals(true)) {
                        headers.put("x-auth-token", sharedPref.getAUTHToken());
                        Log.i("requiresToken ", "This request uses authentication token");
                    }
                    return headers;
                }
            };
        }

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // ADDING REQUEST TO REQUEST QUEUE
    }


}
