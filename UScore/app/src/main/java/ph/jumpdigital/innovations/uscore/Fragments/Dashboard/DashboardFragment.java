package ph.jumpdigital.innovations.uscore.Fragments.Dashboard;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.github.clans.fab.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.Drawer.MainNavigationDrawer;
import ph.jumpdigital.innovations.uscore.Fragments.Graph.GraphMainActivity;
import ph.jumpdigital.innovations.uscore.Fragments.Payment.AddReceiptActivity;
import ph.jumpdigital.innovations.uscore.Fragments.Payment.BillerCenterMainActivity;
import ph.jumpdigital.innovations.uscore.Fragments.Payment.PaymentHistoryActivity;
import ph.jumpdigital.innovations.uscore.Fragments.Visa.AddCreditCardActivity;
import ph.jumpdigital.innovations.uscore.Locator.UnionBranchLocatorActivity;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.PaymentCenterModel;
import ph.jumpdigital.innovations.uscore.Singleton.UserModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.JSonValleyArrayRequest;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.VolleyRequest;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class DashboardFragment extends Fragment implements View.OnClickListener {
    JSONObject jsonObject;
    FrameLayout creditScoreFrame, creditBalance, paymentHistory;
    FloatingActionButton addbill_item, addcreditcard_item;

    TextView creditPoints, accountNum, accountName, availableBal, currentBal, currencyText;
    RelativeLayout ubaccntdetailsholder, noaccntHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.dashboard_main_layout, viewGroup, false);
        noaccntHolder = (RelativeLayout) mainView.findViewById(R.id.noaccntHolder);
        ubaccntdetailsholder = (RelativeLayout) mainView.findViewById(R.id.ubaccntdetailsholder);
        addbill_item = (FloatingActionButton) mainView.findViewById(R.id.addbill_item);
        addcreditcard_item = (FloatingActionButton) mainView.findViewById(R.id.addcreditcard_item);
        addbill_item.setOnClickListener(this);
        addcreditcard_item.setOnClickListener(this);
        creditPoints = (TextView) mainView.findViewById(R.id.creditPoints);
        accountNum = (TextView) mainView.findViewById(R.id.accountNum);
        accountName = (TextView) mainView.findViewById(R.id.accountName);
        availableBal = (TextView) mainView.findViewById(R.id.availableBal);
        currentBal = (TextView) mainView.findViewById(R.id.currentBal);
        currencyText = (TextView) mainView.findViewById(R.id.currencyText);


        creditScoreFrame = (FrameLayout) mainView.findViewById(R.id.creditScoreFrame);
        creditBalance = (FrameLayout) mainView.findViewById(R.id.creditBalance);
        paymentHistory = (FrameLayout) mainView.findViewById(R.id.paymentHistory);
        creditScoreFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), GraphMainActivity.class));
            }
        });

        creditBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UnionBankAccountNumActivity.class));
            }
        });

        paymentHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PaymentHistoryActivity.class));
            }
        });


        fetchUserData();
        return mainView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addcreditcard_item:
                startActivity(new Intent(getActivity(), AddCreditCardActivity.class));
                break;

            case R.id.addbill_item:
                startActivity(new Intent(getActivity(), BillerCenterMainActivity.class));
                break;
        }

    }


    private void fetchUserData() {
        String url = Constants.apiURL + Constants.showUserDetais;
        Constants.volleyTAG = "getuserdata";
        VolleyRequest request = new VolleyRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessObjectListener(),
                createRequestErrorObjectListener(),
                getActivity(), true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity(), "Loading", " ...");

    }


    private void fetchUnionBankAccntData() {

        String url = Constants.UNION_GET_ACCOUNT_URL + UserModel.getUBAccntNum();
        Constants.volleyTAG = "getunionbankaccount";
        JSonValleyArrayRequest request = new JSonValleyArrayRequest(getActivity(),
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                getActivity(), false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonArrayRequest, Constants.volleyTAG);
        CustomUtils.showDialog(getActivity(), "Loading", " ...");

    }

    private Response.ErrorListener createRequestErrorObjectListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Log.i("UnionBank-Accnt", error.toString());
                }

                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessObjectListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                CustomUtils.hideDialog();
                try {
                    Log.d("Paymentgateway-Success", response.toString(4));

                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");
                    if (Constants.status.toString().contentEquals("true")) {
                        if (Constants.volleyTAG.toString().contentEquals("getuserdata")) {


                            UserModel.setEmail(jsonObject.getString("email"));
                            UserModel.setUsername(jsonObject.getString("user_name"));
                            UserModel.setUsername(jsonObject.getString("first_name"));
                            UserModel.setMiddleName(jsonObject.getString("middle_name"));
                            UserModel.setLastName(jsonObject.getString("last_name"));
                            UserModel.setGender(jsonObject.getString("gender"));

                            if (jsonObject.getString("unionbank_account_num") == null) {
                                noaccntHolder.setVisibility(View.VISIBLE);
                            } else {
                                ubaccntdetailsholder.setVisibility(View.VISIBLE);
                                fetchUnionBankAccntData();
                            }

                            int credit_card_pts = jsonObject.getInt("credit_card_points");
                            int payment_gate_pts = jsonObject.getInt("payment_gateway_points");

                            int sum = credit_card_pts + payment_gate_pts;

                            JSONArray paymentGatewayArr = jsonObject.getJSONArray("payment_gateway");

                            for (int i = 0; i < paymentGatewayArr.length(); i++) {
                                JSONObject payGateObj = paymentGatewayArr.getJSONObject(i);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put("dop", payGateObj.getString("date_of_payment"));
                                map.put("biller", payGateObj.getString("biller"));
                                map.put("amount", payGateObj.getString("amount"));
                                PaymentCenterModel.arraylist = new ArrayList<HashMap<String, String>>();
                                PaymentCenterModel.arraylist.add(map);

//                                PaymentCenterModel.arrayList.add(paymentGatewayArr.get(i).toString());
                            }

                            creditPoints.setText(String.valueOf(sum));
                            Log.i("SUM:", String.valueOf(sum));

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

        };
    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Log.i("UnionBank-Accnt", error.toString());
                }

                CustomUtils.hideDialog();
            }
        };
    }


    private Response.Listener<JSONArray> createRequestSuccessListener() {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                CustomUtils.hideDialog();

                try {
                    Log.i("UnionBank-Accnt", response.toString(4));
                    JSONArray unionArray = new JSONArray(String.valueOf(response));
                    for (int i = 0; i < unionArray.length(); i++) {
                        JSONObject unionObj = unionArray.getJSONObject(i);
                        Log.i("UNIONACC-OBJ:", unionObj.toString());
                        PaymentCenterModel.setAccount_name(unionObj.getString("account_no"));
                        PaymentCenterModel.setUaccountNum(unionObj.getString("account_name"));
                        PaymentCenterModel.setCurrency(unionObj.getString("currency"));
                        PaymentCenterModel.setAvaiable_balance(unionObj.getString("avaiable_balance"));
                        PaymentCenterModel.setCurrent_balance(unionObj.getString("current_balance"));


                        accountNum.setText(unionObj.getString("account_no"));
                        accountName.setText(unionObj.getString("account_name"));
                        availableBal.setText(unionObj.getString("avaiable_balance"));
                        currentBal.setText(unionObj.getString("current_balance"));
                        currencyText.setText(unionObj.getString("currency"));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };
    }
}