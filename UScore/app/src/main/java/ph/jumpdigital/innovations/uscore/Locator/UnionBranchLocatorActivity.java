package ph.jumpdigital.innovations.uscore.Locator;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.mikephil.charting.charts.LineChart;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.PaymentCenterModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.JSonValleyArrayRequest;

public class UnionBranchLocatorActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_union_locator_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("Union Bank Branches");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fetchUnionBankBranches();
    }

    private void fetchUnionBankBranches() {
        String url = Constants.UNION_GET_BRANCHES_URL;
        Constants.volleyTAG = "unionbankbranches";
        JSonValleyArrayRequest request = new JSonValleyArrayRequest(UnionBranchLocatorActivity.this,
                Constants.getMethod, url,
                null, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UnionBranchLocatorActivity.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonArrayRequest, Constants.volleyTAG);
        CustomUtils.showDialog(UnionBranchLocatorActivity.this, "Loading", " ...");

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera

    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    Log.i("UnionBank-Accnt", error.toString());
                }
                CustomUtils.hideDialog();
            }
        };
    }


    private Response.Listener<JSONArray> createRequestSuccessListener() {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                CustomUtils.hideDialog();
                try {
                    Log.i("UnionBranches-", response.toString(4));
                    JSONArray unionBranchArray = new JSONArray(String.valueOf(response));
                    for (int i = 0; i < unionBranchArray.length(); i++) {
                        JSONObject unionBranchObj = unionBranchArray.getJSONObject(i);
                        Log.i("unionBranchArray-OBJ:", unionBranchObj.toString());

                        LatLng unionBranches = new LatLng(unionBranchObj.getDouble("latitude"), unionBranchObj.getDouble("longitude"));
                        mMap.addMarker(new MarkerOptions()
                                .position(unionBranches)
                                .snippet(unionBranchObj
                                        .getString("address"))
                                .title(unionBranchObj.getString("name"))
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ubmapmarker)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(unionBranches, 10));
                        // Zoom in, animating the camera.
                        mMap.animateCamera(CameraUpdateFactory.zoomIn());


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
