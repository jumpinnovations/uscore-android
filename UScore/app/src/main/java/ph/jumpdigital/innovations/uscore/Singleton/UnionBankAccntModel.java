package ph.jumpdigital.innovations.uscore.Singleton;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class UnionBankAccntModel {
    public static String getAccount_no() {
        return account_no;
    }

    public static void setAccount_no(String account_no) {
        UnionBankAccntModel.account_no = account_no;
    }

    public static String getCurrency() {
        return currency;
    }

    public static void setCurrency(String currency) {
        UnionBankAccntModel.currency = currency;
    }

    public static String getAccount_name() {
        return account_name;
    }

    public static void setAccount_name(String account_name) {
        UnionBankAccntModel.account_name = account_name;
    }

    public static String getStatus() {
        return status;
    }

    public static void setStatus(String status) {
        UnionBankAccntModel.status = status;
    }

    public static String getAvailable_balance() {
        return available_balance;
    }

    public static void setAvailable_balance(String available_balance) {
        UnionBankAccntModel.available_balance = available_balance;
    }

    public static String getCurrent_balance() {
        return current_balance;
    }

    public static void setCurrent_balance(String current_balance) {
        UnionBankAccntModel.current_balance = current_balance;
    }

    public static String account_no;
    public static String currency;
    public static String account_name;
    public static String status;
    public static String available_balance;
    public static String current_balance;


}
