package ph.jumpdigital.innovations.uscore.Fragments.Payment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.PaymentCenterModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.MultipartRequest;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class AddReceiptActivity extends AppCompatActivity {
    RelativeLayout addReceiptPlaceHolder;
    ImageView imageReceipt;
    TextView addReceiptLbl;
    Button submitReceiptBtn;
    public static JSONObject jsonObject;

    private int TAKE_PHOTO_REQUEST = 1, PICK_IMAGE_REQUEST = 2, RESULT_OK = -1, REQUEST_CROP_ICON = 3;
    Uri filePath;
    private Bitmap bitmap;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_bill_receipt_layout);

        addReceiptPlaceHolder = (RelativeLayout) findViewById(R.id.addReceiptPlaceHolder);
        imageReceipt = (ImageView) findViewById(R.id.imageReceipt);
        addReceiptLbl = (TextView) findViewById(R.id.addReceiptLbl);
        submitReceiptBtn = (Button) findViewById(R.id.submitReceiptBtn);
        addReceiptPlaceHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showFileChooser();
                selectImage();
            }

        });

        submitReceiptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateUserPhoto();
            }
        });

    }


    private void updateUserPhoto() {
        String url = Constants.apiURL + Constants.uploadReceiptImg;

        Constants.volleyTAG = "upload_receipt";
        if (bitmap == null) {
            imageReceipt.buildDrawingCache();
            bitmap = imageReceipt.getDrawingCache();
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id", PaymentCenterModel.getPaymentGatewayID());

        CustomUtils.showDialog(AddReceiptActivity.this, "Loading", "Updating....");
        MultipartRequest mr = new MultipartRequest(AddReceiptActivity.this, url, params, createRequestErrorListener(), createRequestSuccessListener(), bitmap);
        AppController.getInstance().addToRequestQueue(mr, Constants.volleyTAG);
    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {

                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                CustomUtils.hideDialog();
                try {
                    Log.i("Upload-Receipt", response.toString(4));
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");
                    if (Constants.volleyTAG.toString().contentEquals("upload_receipt")) {
                        CustomUtils.showDialog(AddReceiptActivity.this, "", "Image Sucessfuly added");
                        CustomUtils.hideDialog();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(AddReceiptActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, TAKE_PHOTO_REQUEST);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, PICK_IMAGE_REQUEST);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("RequestCode", requestCode + "");

        if (requestCode == TAKE_PHOTO_REQUEST) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }
            try {
                Bitmap bitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        bitmapOptions);

                imageReceipt.setImageBitmap(bitmap);
                imageReceipt.setVisibility(View.VISIBLE);
                addReceiptLbl.setVisibility(View.GONE);

                String path = android.os.Environment
                        .getExternalStorageDirectory()
                        + File.separator
                        + "Phoenix" + File.separator + "default";
                f.delete();
                OutputStream outFile = null;
                File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
                try {
                    outFile = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                    outFile.flush();
                    outFile.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            Intent intent = new Intent("com.android.camera.action.CROP");
            intent.setDataAndType(filePath, "image/*");
            intent.putExtra("crop", "true");
            intent.putExtra("aspectX", 1);
            intent.putExtra("aspectY", 1);
            intent.putExtra("outputX", 150);
            intent.putExtra("outputY", 150);
            intent.putExtra("noFaceDetection", true);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CROP_ICON);

        } else if (requestCode == REQUEST_CROP_ICON) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                bitmap = extras.getParcelable("data");
                Log.i("BITMAPS", bitmap + " ");
                imageReceipt.setImageBitmap(bitmap);
                imageReceipt.setVisibility(View.VISIBLE);
                addReceiptLbl.setVisibility(View.GONE);
            }
        }
    }
}