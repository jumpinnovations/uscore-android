package ph.jumpdigital.innovations.uscore.Drawer;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.json.JSONObject;

import ph.jumpdigital.innovations.uscore.Fragments.Dashboard.DashboardFragment;
import ph.jumpdigital.innovations.uscore.Fragments.Payment.BillerCenterMainActivity;
import ph.jumpdigital.innovations.uscore.Locator.UnionBranchLocatorActivity;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.User.LoginMainActivity;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.SharedPref;
import ph.jumpdigital.innovations.uscore.Utilities.UI.CustomTypefaceSpan;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class MainNavigationDrawer extends AppCompatActivity {
    JSONObject jsonObject;
    Toolbar toolbar;

    DrawerLayout drawerLayout;
    RelativeLayout mDrawerRelative;
    RelativeLayout wholeLayout;
    LinearLayout frame;
    FrameLayout frame_content;
    MenuItem mi;
    NavigationView navigationViews;
    ActionBar actionBar;
    ActionBarDrawerToggle actionBarDrawerToggle;

    SharedPref sharedPref;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_main_layout);
//        sharedPref = new SharedPref(MainNavigationDrawer.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

//        if (CustomUtils.isConnectingToInternet()) {
//            Toast.makeText(MainNavigationDrawer.this, "Connected", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(MainNavigationDrawer.this, "Not Connected", Toast.LENGTH_SHORT).show();
//        }


        mDrawerRelative = (RelativeLayout) findViewById(R.id.sideMenu);
        drawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer_layout);
        frame = (LinearLayout) findViewById(R.id.linear_content);
        wholeLayout = (RelativeLayout) findViewById(R.id.wholeLayout);
        frame_content = (FrameLayout) findViewById(R.id.frame_content);

        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) frame.getLayoutParams();
        frame.setLayoutParams(params);

        navigationViews = (NavigationView) findViewById(R.id.navigation_view);
        if (navigationViews != null) {
            setupNavigationDrawerContent(navigationViews);
        }

        setupNavigationDrawerContent(navigationViews);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (navigationViews.getWidth() * slideOffset);
                int pageWidth = navigationViews.getWidth();
                /**
                 * Animate layout when drawer slide
                 */
//                if (slideOffset <= 1) {
//                    wholeLayout.setTranslationX(moveFactor * slideOffset);
//                }
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                    wholeLayout.setTranslationX(moveFactor);
//                } else {
//                    TranslateAnimation fade_in = new TranslateAnimation(slideOffset, moveFactor, 0.0f, 0.0f);
//                    fade_in.setDuration(0);
//                    fade_in.setFillAfter(true);
//                    wholeLayout.startAnimation(fade_in);
//                    slideOffset = moveFactor;
//                }
//                Log.i("DRAWER", "SLIDE!");
//                Log.i("DRAWER", String.valueOf(slideOffset));
            }
        };

        //calling sync state is necessay or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
        drawerLayout.addDrawerListener(actionBarDrawerToggle);


        Menu m = navigationViews.getMenu();
        for (int i = 0; i < m.size(); i++) {
            mi = m.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();

            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
//                    Log.i("j Menu", String.valueOf(j));
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);

        }

        CustomUtils.loadFragment(new DashboardFragment(), true, MainNavigationDrawer.this);
    }

    private void setupNavigationDrawerContent(final NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
//                        popBackStack();
                        switch (menuItem.getItemId()) {
                            case R.id.item_navigation_drawer_dashboard:
                                menuItem.setChecked(true);
                                CustomUtils.loadFragment(new DashboardFragment(), true, MainNavigationDrawer.this);
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_profile:
                                menuItem.setChecked(true);
//                                startActivity(new Intent(MainNavigationDrawer.this, UnionBranchLocatorActivity.class));
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_branch_locator:
                                menuItem.setChecked(true);
                                startActivity(new Intent(MainNavigationDrawer.this, UnionBranchLocatorActivity.class));
                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_preferences:
                                menuItem.setCheckable(true);

                                drawerLayout.closeDrawer(GravityCompat.START);
                                return true;

                            case R.id.item_navigation_drawer_logout:
                                showLogoutAlert();
                                return true;
                        }

                        return true;
                    }

                });
    }


    private void applyFontToMenuItem(MenuItem mi) {
        Typeface boldFont = Typeface.createFromAsset(getAssets(), "fonts/avenir-demi.otf");
        Typeface lightFont = Typeface.createFromAsset(getAssets(), "fonts/avenir-light.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        CharSequence menuTitle = mi.getTitle();
        if (mi.toString().contains("General")) {
            mNewTitle.setSpan(new CustomTypefaceSpan("", boldFont), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        } else {
            mNewTitle.setSpan(new CustomTypefaceSpan("", lightFont), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            mNewTitle.setSpan(new ForegroundColorSpan(Color.parseColor("#FFFFFF")), 0, menuTitle.length(), 0);
            mi.setTitle(mNewTitle);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void onBackPressed() {
        FragmentManager fm = getFragmentManager();
        Log.i("EntryCount", String.valueOf(fm.getBackStackEntryCount()));
        if (fm.getBackStackEntryCount() > 1) {
            fm.popBackStack();
        } else if (fm.getBackStackEntryCount() == 1) {
            MainNavigationDrawer.super.onBackPressed();
            showLogoutAlert();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void showLogoutAlert() {
        new AlertDialog.Builder(this)
                .setTitle("Logout?")
                .setMessage("Are you sure you want to logout?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        startActivity(new Intent(MainNavigationDrawer.this, LoginMainActivity.class));
                        MainNavigationDrawer.this.finish();
                    }
                }).create().show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

}
