package ph.jumpdigital.innovations.uscore.User;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.UserModel;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.VolleyRequest;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class SignUpMainActivity extends AppCompatActivity {
    JSONObject jsonObject;


    EditText firstNameEdTxt, middleNameEdTxt, lastNameEdTxt, genderEdTxt,
            birthdayEdTxt, emailEdTxt, userNameEdTxt, passWordEdTxt, confirmPassEdTxt;
    Button bdatePicker, signUpButton;
    TextView backtoLoginTxt;

    String firstname, middlename, lastname, username, gender, bday, email, password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        firstNameEdTxt = (EditText) findViewById(R.id.firstNameEdTxt);
        middleNameEdTxt = (EditText) findViewById(R.id.middleNameEdTxt);
        lastNameEdTxt = (EditText) findViewById(R.id.lastNameEdTxt);
        genderEdTxt = (EditText) findViewById(R.id.genderEdTxt);
        birthdayEdTxt = (EditText) findViewById(R.id.birthdayEdTxt);
        emailEdTxt = (EditText) findViewById(R.id.emailEdTxt);
        userNameEdTxt = (EditText) findViewById(R.id.userNameEdTxt);
        passWordEdTxt = (EditText) findViewById(R.id.passWordEdTxt);
        confirmPassEdTxt = (EditText) findViewById(R.id.confirmPassEdTxt);

        bdatePicker = (Button) findViewById(R.id.bdatePicker);
        signUpButton = (Button) findViewById(R.id.signUpButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstname = firstNameEdTxt.getText().toString();
                middlename = middleNameEdTxt.getText().toString();
                lastname = lastNameEdTxt.getText().toString();
                username = userNameEdTxt.getText().toString();
                gender = genderEdTxt.getText().toString();
                bday = birthdayEdTxt.getText().toString();
                email = emailEdTxt.getText().toString();
                password = passWordEdTxt.getText().toString();
                signUpUser();
            }
        });

        backtoLoginTxt = (TextView) findViewById(R.id.backtoLoginTxt);
        backtoLoginTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpMainActivity.this, LoginMainActivity.class));
                SignUpMainActivity.this.finish();
            }
        });

    }


    private void signUpUser() {
        String url = Constants.apiURL + Constants.signUp;
        Constants.volleyTAG = "signup";

        HashMap<String, String> params = new HashMap<>();
        params.put("first_name", firstname);
        params.put("middle_name", middlename);
        params.put("last_name", lastname);
        params.put("user_name", username);
        params.put("email", email);
        params.put("password", password);
        params.put("gender", gender);
        params.put("birthday", bday);

        VolleyRequest request = new VolleyRequest(SignUpMainActivity.this,
                Constants.postMethod, url,
                params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                SignUpMainActivity.this, false, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(SignUpMainActivity.this, "Loading", "Registering user...");

    }


    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                Toast.makeText(SignUpMainActivity.this, error.getLocalizedMessage() + error.getMessage(), Toast.LENGTH_LONG).show();
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Signup-Success", response.toString(4));
                    CustomUtils.hideDialog();
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    Constants.status = response.getBoolean("status");
                    if (Constants.status.toString().contentEquals("true")) {
                        if (Constants.volleyTAG.toString().contentEquals("signup")) {
                            UserModel.setUserID(jsonObject.optString("id"));
                            Log.d("SIGNUP-SAVED:", "ID SAVED");
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        };
    }
}
