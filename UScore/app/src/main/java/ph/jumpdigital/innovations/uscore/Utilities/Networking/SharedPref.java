package ph.jumpdigital.innovations.uscore.Utilities.Networking;

import android.content.Context;
import android.content.SharedPreferences;

import ph.jumpdigital.innovations.uscore.Utilities.Constants;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class SharedPref {

    // Shared Preferences
    SharedPreferences pref;
    // Editor for Shared preferences
    SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name

    // (make variables public to access from outside)
    public static final String KEY_GCM_TOKEN = "GCMToken";
    public static final String KEY_AUTH_TOKEN = "AUTHToken";
    public static final String KEY_USER_NAME = "USERNAME";
    public String KEY_INITIALIZATION = "Initalize";

    public SharedPref(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(Constants.PACKAGE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void removePref() {
        pref = _context.getSharedPreferences(Constants.PACKAGE_NAME, PRIVATE_MODE);
        pref.edit().clear().commit();
    }

    public String getAUTHToken() {
        return pref.getString(KEY_AUTH_TOKEN, null);
    }

    public void setAUTHToken(String token) {
        editor.putString(KEY_AUTH_TOKEN, token);
        editor.commit();
    }

    public String getUserName() {
        return pref.getString(KEY_USER_NAME, null);
    }

    public  void setUserName(String username){
        editor.putString(KEY_USER_NAME, username);
        editor.commit();
    }
}
