package ph.jumpdigital.innovations.uscore.Fragments.Payment;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.PaymentCenterModel;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class PaymentHistoryActivity extends AppCompatActivity {

    ListView paymentHistoryLv;

    ArrayList<HashMap<String, String>> arraylist;
    PaymentHistoryAdapter paymentHistoryAdapter;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_history_mainlv);
        toolbar = (Toolbar) findViewById(R.id.paymentHistoryToolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("Payment History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        paymentHistoryLv = (ListView) findViewById(R.id.paymentHistoryLv);
        arraylist = new ArrayList<HashMap<String, String>>();


        paymentHistoryAdapter = new PaymentHistoryAdapter(PaymentHistoryActivity.this, PaymentCenterModel.arraylist);
        paymentHistoryLv.setAdapter(paymentHistoryAdapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class PaymentHistoryAdapter extends BaseAdapter {
        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>> data;

        public PaymentHistoryAdapter(Context context,
                                     ArrayList<HashMap<String, String>> arraylist) {
            this.context = context;
            data = arraylist;

        }

        public class ViewHolder {
            TextView paymentHistroyDate;
            TextView paymentHistroyCenter;
            TextView paymentHistoryAmount;

            HashMap<String, String> resultp = new HashMap<String, String>();

        }


        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }


        @Override
        public View getView(final int position, View view, ViewGroup viewGroup) {
            final ViewHolder holder;

            if (view == null) {
                holder = new ViewHolder();
                inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.payment_history_list, viewGroup, false);
                holder.paymentHistroyDate = (TextView) view.findViewById(R.id.paymentHistroyDate);
                holder.paymentHistroyCenter = (TextView) view.findViewById(R.id.paymentHistroyCenter);
                holder.paymentHistoryAmount = (TextView) view.findViewById(R.id.paymentHistoryAmount);


            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.resultp = data.get(position);

            view.setTag(holder);


            holder.paymentHistroyDate.setText(holder.resultp.get("dop"));
            holder.paymentHistroyCenter.setText(holder.resultp.get("biller"));
            holder.paymentHistoryAmount.setText(holder.resultp.get("amount"));

            return view;
        }
    }

}
