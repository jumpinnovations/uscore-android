package ph.jumpdigital.innovations.uscore.Utilities.Networking;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.util.CharsetUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import ph.jumpdigital.innovations.uscore.Utilities.Constants;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class MultipartRequest extends Request<JSONObject> {

    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private String FILE_PART_NAME = "files";
    private Bitmap bmp;
    private final Response.Listener<JSONObject> mListener;
    private long fileLength = 0L;
    SharedPref sharedPref;
    Context context;
    HashMap<String, String> params;

    public Map<String, String> getHeaders() throws AuthFailureError {
        // you can add your custom headers here
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("x-auth-token", sharedPref.getAUTHToken());
        return headers;
    }

    public MultipartRequest(Context context, String url, HashMap<String, String> params, Response.ErrorListener errorListener, Response.Listener<JSONObject> listener, Bitmap bmp) {
        super(Constants.putMethod, url, errorListener);
//        HttpsTrustManager.allowAllSSL();
        this.mListener = listener;
        this.bmp = bmp;
        this.context = context;
        this.params = params;

        sharedPref = new SharedPref(context);
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            entity.setCharset(CharsetUtils.get("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        buildMultipartEntity();
        httpentity = entity.build();
    }

    private void buildMultipartEntity() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 10, stream);
        final byte[] bitmapdata = stream.toByteArray();
        ByteArrayBody bab = new ByteArrayBody(bitmapdata, "photo.jpg");

        entity.addPart("image", bab);// if image is comming from billing else use photo_url
        for (Map.Entry<String, String> entry : params.entrySet()) {
            entity.addTextBody(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
        Response<JSONObject> jsonObject = null;
        try {
            try {
                jsonObject = Response.success(new JSONObject(new String(response.data, "UTF-8")), getCacheEntry());

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                jsonObject = Response.success(new JSONObject(new String(response.data)), getCacheEntry());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    @Override
    protected void deliverResponse(JSONObject response) {
        mListener.onResponse(response);
    }

//Override getHeaders() if you want to put anything in header

    public static interface MultipartProgressListener {
        void transferred(long transfered, int progress);
    }

    public static class CountingOutputStream extends FilterOutputStream {
        private final MultipartProgressListener progListener;
        private long transferred;
        private long fileLength;

        public CountingOutputStream(final OutputStream out, long fileLength,
                                    final MultipartProgressListener listener) {
            super(out);
            this.fileLength = fileLength;
            this.progListener = listener;
            this.transferred = 0;
        }

        public void write(byte[] b, int off, int len) throws IOException {
            out.write(b, off, len);
            if (progListener != null) {
                this.transferred += len;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

        public void write(int b) throws IOException {
            out.write(b);
            if (progListener != null) {
                this.transferred++;
                int prog = (int) (transferred * 100 / fileLength);
                this.progListener.transferred(this.transferred, prog);
            }
        }

    }

}
