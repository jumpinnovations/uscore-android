package ph.jumpdigital.innovations.uscore.Singleton;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class UserModel {

    public static String getxAuthToken() {
        return xAuthToken;
    }

    public static void setxAuthToken(String xAuthToken) {
        UserModel.xAuthToken = xAuthToken;
    }

    public static String getUserID() {
        return userID;
    }

    public static void setUserID(String userID) {
        UserModel.userID = userID;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        UserModel.firstName = firstName;
    }

    public static String getMiddleName() {
        return middleName;
    }

    public static void setMiddleName(String middleName) {
        UserModel.middleName = middleName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static void setLastName(String lastName) {
        UserModel.lastName = lastName;
    }

    public static String getGender() {
        return gender;
    }

    public static void setGender(String gender) {
        UserModel.gender = gender;
    }

    public static String getBirthDay() {
        return birthDay;
    }

    public static void setBirthDay(String birthDay) {
        UserModel.birthDay = birthDay;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UserModel.email = email;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        UserModel.username = username;
    }

    public static String getPhotoURL() {
        return photoURL;
    }

    public static void setPhotoURL(String photoURL) {
        UserModel.photoURL = photoURL;
    }

    public static String xAuthToken;
    public static String userID;
    public static String firstName;
    public static String middleName;
    public static String lastName;
    public static String gender;
    public static String birthDay;
    public static String email;
    public static String username;
    public static String photoURL;

    public static String getUBAccntNum() {
        return UBAccntNum;
    }

    public static void setUBAccntNum(String UBAccntNum) {
        UserModel.UBAccntNum = UBAccntNum;
    }

    public static String UBAccntNum;
}
