package ph.jumpdigital.innovations.uscore.Fragments.Dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import ph.jumpdigital.innovations.uscore.AppController;
import ph.jumpdigital.innovations.uscore.Drawer.MainNavigationDrawer;
import ph.jumpdigital.innovations.uscore.R;
import ph.jumpdigital.innovations.uscore.Singleton.UserModel;
import ph.jumpdigital.innovations.uscore.User.LoginMainActivity;
import ph.jumpdigital.innovations.uscore.Utilities.Constants;
import ph.jumpdigital.innovations.uscore.Utilities.CustomUtils;
import ph.jumpdigital.innovations.uscore.Utilities.Networking.VolleyRequest;

import static ph.jumpdigital.innovations.uscore.Utilities.Constants.status;

/**
 * Created by vidalbenjoe on 28/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class UnionBankAccountNumActivity extends AppCompatActivity{
    public static JSONObject jsonObject;
    EditText UBaccountNumEdt;
    Button submitAccntNum;
    Toolbar toolbar;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_union_bankaccnt_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setTitle("Union Bank Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        UBaccountNumEdt = (EditText) findViewById(R.id.UBaccountNumEdt);
        submitAccntNum = (Button) findViewById(R.id.submitAccntNum);
        submitAccntNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addUBAccount();
            }
        });
    }


    private void addUBAccount(){
        String url = Constants.apiURL + Constants.add_union_accnt_num;
        HashMap<String, String> params = new HashMap<>();
        params.put("id", UserModel.getUserID());
        params.put("unionbank_account_num", UBaccountNumEdt.getText().toString());

        Constants.volleyTAG = "add_accntnum";
        VolleyRequest request = new VolleyRequest(UnionBankAccountNumActivity.this,
                Constants.putMethod, url,
                params, null,
                createRequestSuccessListener(),
                createRequestErrorListener(),
                UnionBankAccountNumActivity.this, true, Constants.volleyTAG);
        AppController.getInstance().addToRequestQueue(request.jsonObjectRequest, Constants.volleyTAG);
        CustomUtils.showDialog(UnionBankAccountNumActivity.this, "Loading", "Verifying account num");
    }
    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("VolleyError", error.getLocalizedMessage() + error.getMessage());
                try {
                    if (error.networkResponse != null) {
                        CustomUtils.hideDialog();
                        String responseBody = new String(error.networkResponse.data, "UTF-8");
                        jsonObject = new JSONObject(responseBody);
                        JSONArray errorResult = jsonObject.getJSONArray("error");
                        //get errror by index
                        for (int i = 0; i < errorResult.length(); i++) {
                            JSONObject errorObj = errorResult.getJSONObject(i);
                            Iterator keys = errorObj.keys();
                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String currentDynamicKey = (String) keys.next();
                                // get the value of the dynamic key
                                String currentDynamicValue = errorObj.getString(currentDynamicKey);
                                Toast.makeText(UnionBankAccountNumActivity.this, currentDynamicValue, Toast.LENGTH_SHORT).show();
                            }
                        }
                        Log.e("jsonObject", errorResult + "");
                    } else {
                        Toast.makeText(UnionBankAccountNumActivity.this, "Please check your internet connection",
                                Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("errorJSON", "" + e);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    Log.e("errorEncoding", "" + e);
                }
                CustomUtils.hideDialog();
            }
        };
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Log.d("Login-Success", response.toString(4));
                    jsonObject = new JSONObject(String.valueOf(response.getJSONObject("results")));
                    status = response.getBoolean("status");

                    if (status.toString().contentEquals("true")) {
                        UserModel.setUBAccntNum(jsonObject.getString("unionbank_account_num"));
                        onBackPressed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
