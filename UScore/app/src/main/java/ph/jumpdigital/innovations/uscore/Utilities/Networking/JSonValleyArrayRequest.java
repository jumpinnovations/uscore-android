package ph.jumpdigital.innovations.uscore.Utilities.Networking;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import ph.jumpdigital.innovations.uscore.Utilities.Constants;

/**
 * Created by vidalbenjoe on 28/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class JSonValleyArrayRequest {
    Context context;
    public JsonObjectRequest jsonObjectRequest;
    public JsonArrayRequest jsonArrayRequest;
    SharedPref sharedPref;
    public static Boolean isUnionBank;

    public JSonValleyArrayRequest(Context context, int method, String url, HashMap<String, String> params, JSONArray jsonParams, Response.Listener<JSONArray> reponseListener,
                                  Response.ErrorListener errorListener, Activity activity, final Boolean requiredToken, final String volleyTAG) {
        this.context = context;
        sharedPref = new SharedPref(context);
        if (params != null) {
            Log.i("VolleyHashMap", params.toString());
        } else {

        }

        if (volleyTAG.equals("getunionbankaccount") || volleyTAG.equals("unionbankbranches")) {
            //THIS IS FOR METHOD GET
            jsonArrayRequest = new JsonArrayRequest(url, reponseListener
                    , errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-type", "application/json");
                    headers.put("Accept", "application/json");
                    headers.put("x-ibm-client-id", Constants.UNION_CLIENT_ID);
                    headers.put("x-ibm-client-secret", Constants.UNION_CLIENT_SECRET_KEY);


                    return headers;
                }
            };
        }

        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(20000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // ADDING REQUEST TO REQUEST QUEUE
    }
}
