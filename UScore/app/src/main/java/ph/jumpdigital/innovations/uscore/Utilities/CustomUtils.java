package ph.jumpdigital.innovations.uscore.Utilities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;

import ph.jumpdigital.innovations.uscore.R;

/**
 * Created by vidalbenjoe on 27/08/2016.
 * Jump Digital Inc. Innovation Team
 */

public class CustomUtils {
    private static Context _context;
    static ProgressDialog mProgressDialog;

    public CustomUtils(Context context) {
        _context = context;
    }



    /**
     * Display progress dialog
     *
     * @param context
     */
    public static void showDialog(Context context, String title, String message) {
        mProgressDialog = new ProgressDialog(context);
        // Set progressdialog title
        mProgressDialog.setTitle(title);
        // Set progressdialog message
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();
    }

    /**
     * Hide Dialog
     */
    public static void hideDialog() {
        // Close the progressdialog
        if (mProgressDialog != null)
            mProgressDialog.dismiss();
        mProgressDialog.setCancelable(true);
//        mProgressDialog.cancel();
    }

    /**
     * Display Toast
     *
     * @param activity
     * @param message
     */
    public static void toastMessage(Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }





    /**
     * Method is used for checking valid email id format.
     *
     * @return boolean true for valid false for invalid
     */

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }


    /**
     * This method is used for checking Alphanumeric characters :)
     *
     * @param edit, filter
     */
    public static void AlphaNumFilter(EditText edit, InputFilter filter) {
        filter = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                try {
                    Character c = source.charAt(0);
                    if (Character.isLetter(c) || Character.isDigit(c)) {
                        return "" + Character.toUpperCase(c);
                    } else {
                        //not alphanumeric
                        Log.i("Alpha", "not alphanumeric");
                        return "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        edit.setFilters(new InputFilter[]{filter});
    }

    /**
     * This method is used for managing fragment transaction
     *
     * @param fragment
     * @param addToBackStack
     * @param context
     */

    public static void loadFragment(final Fragment fragment, boolean addToBackStack, Context context) {
        final FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_content, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
//        CustomUtils.popBackStack(fragment);
        fragmentTransaction.commit();
    }


}
